# Les 2 Git introductie
Git is ontstaan als opensource variant op vergelijkbare gesloten software in die tijd, het is gemaakt door de Linus Trovalds, de grondlegger van Linux. Git is een zogenaamde source management tool, die het mogelijk maakt lokaal op je laptop gemaakte code centraal neer te zetten en op basis van standaard handelingen in en uit te checken. Hiermee wordt bedoeld dat je code bij het centraal neerzetten wordt geinspecteerd op overlap in wijzigingen en wordt er administratief gelogd wanneer en door wie een wijziging heeft plaatsgevonden. De software houdt bij welke gedeeltes binnen de folder structuur gewijzigd zijn en waarschuwt ook voor conflicten met de centrale versie (vaak Master genoemd). 

## Git en GitLab samen
GitLab is zoals we eerder zagen en centrale opslag voor code en maakt het mogelijke om via Git te werken, ipv het knippen en plakken of editten in de browser, zoals we in Les 1 hebben gedaan. Genoeg tekst en uitleg voor nu, laten we beginnen


# Installatie
Allereerst hebben we een versie van Git nodig op de lokale PC waarop we werken. Hieronder vind je de instructies om hiermee direct te starten.

## Installatie Windows
Voor Windows gebruiker maken we gebruik van een Git versie die GitBash genoemdwordt. Deze GitBash zorgt ervoor dat we voor deze cursus alle commando's gelijk kunnen houden voor alle besturingssystemen. 

1.  Download GitBash
2.  Installeer GitBash
3.  Start GitBash

Type: 

`git --version`

Volg de rest van Les 2, onder de instructies van de Linux installatie..

## Installatie Mac
Ga naar de volgende website en volg de instructies:
[https://brew.sh/](https://brew.sh/)

Hiemree installeer je HomeBrew, een commando om het installeren van software eenvoudiger te maken op de Mac:

1. Open een Terminal op je mac
1. Knip en plak de installatie regel van de website en druk op Enter
1. Volg de instructies

Controleer of de installatie succesvol was type, gevolgd door Enter:

`brew doctor`

Type, om installatie updates uit te voeren:

`brew update`

Type, op Git te installeren:

`brew install git`

In de terminal type je dan:

`git --version`

Dit laat zien dat Git op je Mac staat en welke versie je hebt.

Volg de rest van Les 2, onder de instructies van de Linux installatie..


## Installatie Linux en Chromebook
Op Ubuntu en afgeleiden type je het volgende in een Terminal:

`sudo -s`

Type daarna:

`apt-get update`

`apt-get dist-upgrade -y`

`apt-get install git`

# Configuratie 
Om Git in combinatie met GitLab te gebruiken moeten we eerst zorgen dat er een aantal basisinstellingen goed staan op zowel je lokale machine als in GitLab.

## Beveiliging
Als eerste zorgen we ervoor dat de veiligheid van je repository geregeld wordt door te werken op basis van geauthentiseerde verbinding. Deze vorm van authenticatie werkt op basis van public en privat keypairs, die je zelf aanmaakt op je computer.

### SSH Key pair aanmaken
In de Treminal type je, gevolgd door Enter:

`ssh-keygen`

Hieronder staan de instructies bij eerste gebruik van ssh-keygen:
1.  Volg de instructies, als eerste hoef je geen naam op te geven, standaard heten de bestanden dan id_rsa.pub en id_rsa. 
2.  Bij passphrase hoef je niets in te typen, voor deze uitleg, in de praktijk is het wel aan te bevelen, maar voor nu typen we hier ook Enter.
3.  Hierna worden er twee bestanden gemaakt. Om het .pub bestand (de publieke key) uit te lezen type je volgende:

`cat .ssh/id_rsa.pub`

Er verschijnt een lange tekst, met op de plek van ----- nog veel meer tekst eindigend met je gebruikersnaam en/of email:

ssh-rsa AAAAB-----W2Pw== mkeut@DESKTOP-XYZ

Mocht dit niet het geval zijn dan staat je key-pair waarschijnlijk op een andere plek, zoek dan even in de folder structuur naar het key-pair en pas de instructie telkens aan naar dit pad, of volg de instructie opnieuw en kijk goed waar het bestand neergezet wordt.

Om deze tekst eenvoudig te kopieeren type je het volgende (dit is hetzelfde effect als Ctrl-C terwijl de tekst geselecteerd is):

`cat ~/.ssh/id_rsa.pub | clip`

Daarna gaan je naar de GitLab omgeving in je browser.
Om deze sleutel bekend te maken bij gitlab voer je het volgende uit in je GitLab account:

1.  Klik op je ouser icoontje, rechtsboven.
2.  Ga naar Settings.
3.  Klik op SSH Keys aan de linker kant.
4.  Ga in hetb grote venster staan en plak hier je key tekst door 'Ctrl-V' te typen
5.  Geef in tekstveld eronder nog een andere naam aan je sleutel om te onhouden op welke omgeving deze staat.
6.  en klik op Add Key.
7.  Ga terug naar je Terminal en volg de instructies hieronder.

### Testen SSH Key
Om de verbinding met GitLab te testen type je het volgende commando:

`ssh -T git@gitlab.com`

Hierna verschijnt de volgende tekst:
'Welcome to GitLab, @......!'

Mocht je afgeweken hebben van de instructies of al een bestaande lokaal key pair willen gebruiken volg dan de onderstaande tekst.

### Toevoegen SSH Key

`eval $(ssh-agent -s)`

Type volgende, met als locatie je privat key 
`ssh-add ~/.ssh/mijn_eigen-privat_key_zonder_de_pub_toevoeging`





